<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    public function getAll()
    {
        return User::orderBy('created_at', 'DESC')->get();
    }

    public function getByEmail(Request $request)
    {
        $email = $request->input('email');

        $users = User::where('email', $email);

        return $users->first();
    }

    public function getOne($id)
    {
        return User::findOrFail($id);
    }

    public function create(Request $request)
    {
        return User::create([
            'email'         => $request->input('email'),
            'first_name'    => $request->input('first_name'),
            'last_name'     => $request->input('last_name'),
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->email        = $request->input('email');
        $user->first_name   = $request->input('first_name');
        $user->last_name    = $request->input('last_name');

        $user->save();

        return $user;
    }

    public function deleteOne($id)
    {
        return User::destroy($id);
    }
}
