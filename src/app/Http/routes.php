<?php

use App\Models\Article;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// App version
$app->get('/', function () use ($app) {
    return $app->version();
});

// Health check URL
$app->get('health', function () use ($app) {
    return 'OK';
});

// Application URLs

$app->get('users', 'UsersController@getAll');

$app->get('users/login', 'UsersController@getByEmail');

$app->get('users/{id}', 'UsersController@getOne');

$app->post('users', 'UsersController@create');

$app->put('users/{id}', 'UsersController@update');

$app->delete('users/{id}', 'UsersController@deleteOne');

//$app->delete('users', 'UsersController@deleteAll');
